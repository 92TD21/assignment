const Accounts = require('./app/controllers/accounts');
const Assets = require('./app/controllers/assets');
const Tweets = require('./app/controllers/tweet');
const Users = require('./app/controllers/users');
const Admins = require('./app/controllers/admins');

module.exports = [

  { method: 'GET', path: '/', config: Accounts.main },
  { method: 'GET', path: '/signup', config: Accounts.signup },
  { method: 'GET', path: '/login', config: Accounts.login },
  { method: 'POST', path: '/login', config: Accounts.authenticate },
  { method: 'POST', path: '/register', config: Accounts.register },
  { method: 'GET', path: '/logout', config: Accounts.logout },
  { method: 'GET', path: '/settings', config: Accounts.viewSettings },
  { method: 'POST', path: '/settings', config: Accounts.updateSettings },

  { method: 'GET', path: '/user', config: Users.findOne },
  { method: 'GET', path: '/registeredusers', config: Users.viewAllUsers },
  { method: 'GET', path: '/friends', config: Users.viewFriends },
  { method: 'GET', path: '/otherUser', config: Users.viewOtherUsers },
  { method: 'GET', path: '/follow/{id}', config: Users.follow },
  { method: 'GET', path: '/unfollow/{id}', config: Users.unfollow },
  { method: 'POST', path: '/status', config: Users.changeStatus },

  { method: 'GET', path: '/tweetAdmin', config: Admins.viewTweetAdminRights },
  { method: 'GET', path: '/userAdmin', config: Admins.viewUserAdminRights },
  { method: 'GET', path: '/admin', config: Admins.viewAdminRights },
  { method: 'GET', path: '/addUser', config: Admins.viewAddUser },
  { method: 'GET', path: '/addUserForMaster', config: Admins.viewAddUserForMaster },
  { method: 'POST', path: '/addUser', config: Admins.addUser },
  { method: 'POST', path: '/addUserFromMaster', config: Admins.addUserFromMaster },
  { method: 'GET', path: '/removeUser', config: Admins.viewRemoveUser },
  { method: 'GET', path: '/delete/{id}', config: Admins.removeUser },
  { method: 'GET', path: '/timelineBulk', config: Admins.viewTimelineBulk },
  { method: 'POST', path: '/deleteSome', config: Admins.deleteSomeTweets },

  { method: 'GET', path: '/timeline', config: Tweets.timeline },
  { method: 'GET', path: '/tweet', config: Tweets.viewTweets },
  { method: 'GET', path: '/userTimeline', config: Tweets.viewUserTimeline },
  { method: 'GET', path: '/userTimeline/{id}', config: Tweets.viewSingleUserTimeline },
  { method: 'GET', path: '/friendsTimeline', config: Tweets.viewFriendsTimeline },
  { method: 'POST', path: '/tweet', config: Tweets.tweet },
  { method: 'POST', path: '/deleteAll', config: Tweets.deleteAllUserTweets },
  { method: 'GET', path: '/deleteTweet/{id}', config: Tweets.deleteOneTweet },

  {
    method: 'GET',
    path: '/{param*}',
    config: { auth: false },
    handler: Assets.servePublicDirectory,
  },

];
