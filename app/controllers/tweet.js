'use strict';

const Tweet = require('../models/tweets');
const User = require('../models/user');
const Joi = require('joi');

exports.viewTweets = {
  handler: function (request, reply) {
    reply.view('tweet', { title: 'Create a Tweet' });
  },
};

exports.timeline = {
  handler: function (request, reply) {
      Tweet.find({}).populate('tweetUser').then(allTweets => {
        reply.view('timeline',
            { title: 'Tweet Timeline',
            tweets: allTweets,
          });
      }).catch(err => {
        reply.redirect('/');
      });
    },
};

exports.viewUserTimeline = {
    handler: function (request, reply) {
      let userEmail = request.auth.credentials.loggedInUser;
      User.findOne({ email: userEmail }).then(foundUser => {
        Tweet.find({ tweetUser: foundUser._id }).populate('tweetUser').then(allUserTweets => {
          let number = 0;
          let follow = foundUser.following.length;
          let followers = foundUser.followers;
          let tweeter = foundUser.firstName + ' ' + foundUser.lastName;
          let status = foundUser.status;
          let month = foundUser.joined.getMonth() + 1;
          if (month < 10) {
            month = '0' + month;
          }
          let joined = foundUser.joined.getDate() + '-' + month + '-' + foundUser.joined.getFullYear();
          if (allUserTweets !== null) {
            number = allUserTweets.length;
          }

          reply.view('userTimeline',
              { title: 'Tweet Timeline',
                tweets: allUserTweets,
                tweetNumber: number,
                follow: follow,
                followers: followers,
                tweeter: tweeter,
                status: status,
                joined: joined,
              });
        }).catch(err => {
          reply.redirect('/');
        });
      }).catch(err => {
        reply.redirect('/');
      });
    },
  };

exports.viewSingleUserTimeline = {
  handler: function (request, reply) {
    User.findOne({ _id: request.params.id }).then(foundUser => {
      Tweet.find({ tweetUser: foundUser._id }).populate('tweetUser').then(allUserTweets => {
        let number = 0;
        let follow = foundUser.following.length;
        let followers = foundUser.followers;
        let tweeter = foundUser.firstName + ' ' + foundUser.lastName;
        let status = foundUser.status;
        let month = foundUser.joined.getMonth() + 1;
        if (month < 10) {
          month = '0' + month;
        }

        let joined = foundUser.joined.getDate() + '-' + month + '-' + foundUser.joined.getFullYear();
        if (allUserTweets !== null) {
          number = allUserTweets.length;
        }

        reply.view('userTimeline',
            { title: 'Tweet Timeline',
              tweets: allUserTweets,
              tweeter: tweeter,
              tweetNumber: number,
              follow: follow,
              followers: followers,
              status: status,
              joined: joined,
            });
      }).catch(err => {
        reply.redirect('/');
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

exports.viewFriendsTimeline = {
  handler: function (request, reply) {
    let userEmail = request.auth.credentials.loggedInUser;
    let tweet = new Array();
    User.findOne({ email: userEmail }).then(user => {
      let foundUser = user.following;
      user.following.forEach(currentUser => {
        Tweet.find({ tweetUser: currentUser }).populate('tweetUser').then(friendsTweet => {
          tweet = tweet.concat(friendsTweet);
        });
      });
      let mili = 500;
      setTimeout(function () {
        reply.view('friendsTimeline',
            { title: 'Tweet Timeline',
              tweets: tweet,
            });
      }, mili);

    }).catch(err => {
      reply.redirect('/');
    });
  },
};

exports.tweet = {

  validate: {

    payload: {
      tweet: Joi.string().required(),
    },

    options: {
      abortEarly: false,
    },

    failAction: function (request, reply, source, error) {
      User.find({}).then(users => {
        reply.view('tweet', {
          title: 'Invalid Tweet',
          users: users,
          errors: error.data.details,
        }).code(400);
      }).catch(err => {
        reply.redirect('/');
      });
    },
  },

  handler: function (request, reply) {
    var userEmail = request.auth.credentials.loggedInUser;
    let userId = null;
    let tweet = null;
    User.findOne({ email: userEmail }).then(user => {
      let data = request.payload;
      userId = user._id;
      tweet = new Tweet(data);
      tweet.tweetUser = userId;
      return tweet.save();
    }).then(newTweet => {
      reply.redirect('/timeline');
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

exports.deleteAllUserTweets = {
  handler: function (request, reply) {
    let userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).then(foundUser => {
      Tweet.remove({ tweetUser: foundUser._id }).then(allUserTweets => {
        reply.redirect('/userTimeline');
      }).catch(err => {
        reply.redirect('/');
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

exports.deleteOneTweet = {
  handler: function (request, reply) {
        let userEmail = request.auth.credentials.loggedInUser;
        let admin = request.auth.credentials.loggedInTweetAdmin;
        let master = request.auth.credentials.loggedInMasterAdmin;
        Tweet.findOne({ _id: request.params.id }).then(check => {
          User.findOne({ _id: check.tweetUser }).then(foundUser => {
            if (userEmail === foundUser.email || admin === true || master === true) {
              Tweet.remove({ _id: request.params.id }).then(UserTweets => {
                reply.redirect('/timeline');
              }).catch(err => {
                reply.redirect('/');
              });
            } else {
              Tweet.find({}).populate('tweetUser').then(allTweets => {
                reply.view('timeline',
                    { title: 'Tweet Timeline',
                      tweets: allTweets,
                      message: 'You neither have an admin account nor it is your own tweet',
                    });
              }).catch(err => {
                reply.redirect('/');
              });
            }
          }).catch(err => {
            reply.redirect('/');
          });
        }).catch(err => {
          reply.redirect('/');
        });
      },
};
