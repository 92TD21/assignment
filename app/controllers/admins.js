'use strict';
const User = require('../models/user');
const Tweet = require('../models/tweets');
const Joi = require('joi');

exports.viewAdminRights = {
  handler: function (request, reply) {
    let masterAdmin = request.auth.credentials.loggedInMasterAdmin;
    if (masterAdmin === false) {
      reply.redirect('/userTimeline');
    } else {
      reply.view('admin');
    }
  },
};

exports.viewUserAdminRights = {
  handler: function (request, reply) {
    let userAdmin = request.auth.credentials.loggedInUserAdmin;
    let masterAdmin = request.auth.credentials.loggedInMasterAdmin;
    if (userAdmin === false && masterAdmin === false) {
      reply.redirect('/userTimeline');
    } else {
      reply.view('userAdmin');
    }
  },
};
exports.viewTweetAdminRights = {
  handler: function (request, reply) {
    let tweetAdmin = request.auth.credentials.loggedInTweetAdmin;
    let masterAdmin = request.auth.credentials.loggedInMasterAdmin;
    if (tweetAdmin === false && masterAdmin === false) {
      reply.redirect('/userTimeline');
    } else {
      reply.view('tweetAdmin');
    }
  },
};

exports.viewAddUser = {
  handler: function (request, reply) {
    let userAdmin = request.auth.credentials.loggedInUserAdmin;
    let masterAdmin = request.auth.credentials.loggedInMasterAdmin;
    if (userAdmin === false && masterAdmin === false) {
      reply.redirect('/userTimeline');
    } else {
      reply.view('AddUser');
    }
  },
};

exports.viewAddUserForMaster = {
  handler: function (request, reply) {
    let masterAdmin = request.auth.credentials.loggedInMasterAdmin;
    if ( masterAdmin === false) {
      reply.redirect('/userTimeline');
    } else {
      reply.view('addUserForMaster');
    }
  },
};

exports.addUser = {
  auth: false,

  validate: {
    payload: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      status: Joi.string().required(),
      useradmin: Joi.string().required(),
      password: Joi.string().required(),
    },
    options: {
      abortEarly: false,
    },
    failAction: function (request, reply, source, error) {
      reply.view('signup', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },
  },

  handler: function (request, reply) {
    const user = new  User(request.payload);
    user.followers = 0;
    user.tweetadmin = false;
    user.masteradmin = false;
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }

    if (mm < 10) {
      mm = '0' + mm;
    }

    today = mm + '/' + dd + '/' + yyyy;
    user.joined = today;

    user.save().then(newUser => {
      reply.redirect('/userAdmin');
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

exports.addUserFromMaster = {
  auth: false,

  validate: {
    payload: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      status: Joi.string().required(),
      masteradmin: Joi.string().required(),
      tweetadmin: Joi.string().required(),
      useradmin: Joi.string().required(),
      password: Joi.string().required(),
    },
    options: {
      abortEarly: false,
    },
    failAction: function (request, reply, source, error) {
      reply.view('signup', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },
  },

  handler: function (request, reply) {
    const user = new  User(request.payload);
    user.followers = 0;
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }

    if (mm < 10) {
      mm = '0' + mm;
    }

    today = mm + '/' + dd + '/' + yyyy;
    user.joined = today;

    user.save().then(newUser => {
      reply.redirect('/userAdmin');
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

exports.viewRemoveUser = {
  handler: function (request, reply) {
    let userAdmin = request.auth.credentials.loggedInUserAdmin;
    let masterAdmin = request.auth.credentials.loggedInMasterAdmin;
    if (userAdmin === false && masterAdmin === false) {
      reply.redirect('/userTimeline');
    } else {
      User.find({}).populate('users').then(foundUser => {
        reply.view('removeUser',
            { title: 'Remove Users',
              users: foundUser,
              message: '',
            });
      }).catch(err => {
        reply.redirect('/');
      });
    }
  },
};

exports.removeUser = {
  handler: function (request, reply) {
    let userAdmin = request.auth.credentials.loggedInUserAdmin;
    let masterAdmin = request.auth.credentials.loggedInMasterAdmin;
    if (userAdmin === true || masterAdmin === true) {
      User.findOne({ _id: request.params.id }).populate('following').then(deleteUser => {
        for (let i = 0; i < deleteUser.following.length; i++) {
          User.findOne({ _id: deleteUser.following[i]._id }).then(currentUser => {
            currentUser.followers = currentUser.followers - 1;
            currentUser.save();
          }).catch(err => {
            reply.redirect('/');
          });
        }

        Tweet.remove({ tweetUser: deleteUser._id }).then(allUserTweets => {
        }).catch(err => {
          reply.redirect('/');
        });

        User.find({}).populate('following').then(unfollowUser => {
          unfollowUser.forEach(current => {
            if (current.following.toString().includes(deleteUser._id) === true) {
              let j = null;
              for (let i = 0; i < current.following.length; i++) {
                if (current.following[i].email === deleteUser.email) {
                  j = i;
                  break;
                }
              }

              if (j != null) {
                current.following.splice(j, 1);
                current.save();
              }

            }
          });
        }).catch(err => {
          reply.redirect('/');
        });

        let mili = 1000;
        setTimeout(function () {
          User.remove({ _id: request.params.id }).then(user => {
            reply.redirect('/removeUser');
          }).catch(err => {
            reply.redirect('/');
          });
        }, mili);
      }).catch(err => {
        reply.redirect('/');
      });
    } else {
      reply.redirect('/');
    }

  },

};

exports.viewTimelineBulk = {
  handler: function (request, reply) {
    let tweetAdmin = request.auth.credentials.loggedInTweetAdmin;
    let master = request.auth.credentials.loggedInMasterAdmin;
    if (tweetAdmin === false && master === false) {
      reply.redirect('/userTimeline');
    } else {
      Tweet.find({}).populate('tweetUser').then(allTweets => {
        reply.view('timelineBulk',
            { title: 'Tweet Timeline',
              tweets: allTweets,
            });
      }).catch(err => {
        reply.redirect('/');
      });
    }
  },
};

exports.deleteSomeTweets = {
  handler: function (request, reply) {
    let remove  = request.payload;
    if (Array.isArray(remove.public) === true) {
      for (let i = 0; i < remove.public.length; i++) {
        Tweet.remove({ _id: remove.public[i] }).then(removedTweet => {
        }).catch(err => {
          reply.redirect('/');
        });
      }
    } else {
      Tweet.remove({ _id: remove.public }).then(removedTweet => {
      }).catch(err => {
        reply.redirect('/');
      });
    }

    reply.redirect('/timelineBulk');
  },
};

