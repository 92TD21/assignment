'use strict';

const mongoose = require('mongoose');

const tweetsSchema = mongoose.Schema({
  tweet: String,
  tweetUser: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
});

const Tweet = mongoose.model('Tweet', tweetsSchema);
module.exports = Tweet;