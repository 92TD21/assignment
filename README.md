# README #

This README documents the Student name and id, a brief non-technical description, a link to repository hosting the code, a link to the deployed web app and a youtube link to show a tour of the application.

### Student name and id ###

Student name: Thomas Trottmann
Id: 30025284

### Non-technical description ###
First the user has to press the sign up button, then he is able to create an twitter account. When the account is created, the user is able to login.

Afer he has logged in he is able to see the menu at the top of the page. There he has multiple options:
On the dropdown menu users: The user is able to see all users, friends the user is already following, other users the user is not following. There the user has the option to follow or unfollow other users.
In the main menu at the top he also can search for users to follow or unfollow them.

On the dropdown menu timeline,the user can choose between three different timelines: 
A global timeline with all user tweets,
his personal timeline,
timeline with all tweets from his friends,
he is also able to watch the timeline of any other user

On the tweet button at the menu the user is able to update the status, to create a tweet or to delete all his personal tweets
If the user presses the settings button he is able to edit the personal account settings.

The admin features:
An admin is also a normal user.
If it's an user admin, the admin has ability to create new users and delete existing users if the admin presses on admin on the main menu and then the admin has to choose user admin.

If it's an admin for tweets, the admin is able to delete all user tweets individually or in bulk. 
To delete the tweets individually the admin has multiple options. The first one is to delete them directly from each timeline.
The second possibility is to press on the tweet admin view. There he is able to delete the tweets in bulk or individually.

The master admin has the permissions of the user admin and the tweet admin


### Link to repository ###

* Git repository with tagged releases:
* https://bitbucket.org/92TD21/assignment/overview

### Link to the deployment video tour of the application ###

* Heroku Link:
* https://sheltered-reef-87535.herokuapp.com

### Link to the short video tour of the application ###
* Youtube Link:
* https://www.youtube.com/watch?time_continue=3&v=RJmXE0VLu18