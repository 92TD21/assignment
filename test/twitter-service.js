'use strict';

const SyncHttpService = require('./sync-http-service');
const baseUrl = 'http://localhost:4000';

class TwitterService {

  constructor(baseUrl) {
    this.httpService = new SyncHttpService(baseUrl);
  }

  getUsers() {
    return this.httpService.get('/api/user');
  }

  getUser(id) {
    return this.httpService.get('/api/user/' + id);
  }

  createUser(newUser) {
    return this.httpService.post('/api/user', newUser);
  }

  deleteAllUsers() {
    return this.httpService.delete('/api/user');
  }

  deleteOneUser(id) {
    return this.httpService.delete('/api/user/' + id);
  }

}

module.exports = TwitterService;
