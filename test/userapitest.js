'use strict';


const assert = require('chai').assert;
const TwitterService = require('./twitter-service');
const fixtures = require('./fixtures.json');
const _ = require('lodash');

suite('Candidate API tests', function () {

  let users = fixtures.users;
  let newUser = fixtures.newUser;

  const twitterService = new TwitterService('http://localhost:4000');

  beforeEach(function () {
    twitterService.deleteAllUsers();
  });

  afterEach(function () {
    twitterService.deleteAllUsers();
  });

  test('create a user', function () {
    const returnedUser = twitterService.createUser(newUser);
    assert(_.some([returnedUser], newUser), 'returnedUser must be a superset of newUser');
    assert.isDefined(returnedUser._id);
  });

  test('get user', function () {
    const c1 = twitterService.createUser(newUser);
    const c2 = twitterService.getUser(c1._id);
    assert.deepEqual(c1, c2);
  });

  test('get invalid user', function () {
    const c1 = twitterService.getUser('1234');
    assert.isNull(c1);
    const c2 = twitterService.getUser('012345678901234567890123');
    assert.isNull(c2);
  });

  test('delete a User', function () {
    this.timeout(5000);
    const c = twitterService.createUser(newUser);
    assert(twitterService.getUser(c._id) != null);
    twitterService.deleteOneUser(c._id);
    assert(twitterService.getUser(c._id) == null);
  });

  test('get all User', function () {
    this.timeout(5000);
    for (let c of users) {
      twitterService.createUser(c);
    }

    const allUsers = twitterService.getUsers();
    assert.equal(allUsers.length, users.length);
  });

  test('get users detail', function () {
    this.timeout(5000);
    for (let c of users) {
      twitterService.createUser(c);
    }

    const allUsers = twitterService.getUsers();
    for (var i = 0; i < users.length; i++) {
      assert(_.some([allUsers[i]], users[i]), 'returnedUser must be a superset of newUser');
    }
  });

  test('get all Users empty', function () {
    const allUsers = twitterService.getUsers();
    assert.equal(allUsers.length, 0);
  });
});