'use strict';
const User = require('../models/user');
const Joi = require('joi');

exports.findOne = {
  handler: function (request, reply) {
    const searchedUser =  request.url.query.user.split(' ');
    if (searchedUser[1] === undefined) {
      User.find({ firstName: searchedUser[0] }).populate('users').then(foundUser => {
        if (foundUser.length > 0) {
          reply.view('registeredusers',
              {
                title: 'Searched User',
                users: foundUser,
              });
        } else {
          User.find({ lastName: searchedUser[0] }).populate('users').then(foundUser => {
            if (foundUser.length > 0) {
              reply.view('registeredusers',
                  { title: 'Searched User',
                    users: foundUser,
                  });
            } else {
              User.find({ email: searchedUser[0] }).populate('users').then(foundUser => {
                reply.view('registeredusers',
                    { title: 'Searched User',
                      users: foundUser,
                    });
              }).catch(err => {
                reply.redirect('/');
              });
            }
          }).catch(err => {
            reply.redirect('/');
          });
        }
      });
    } else {
      User.find({ firstName: searchedUser[0], lastName: searchedUser[1] }).populate('users').then(foundUser => {
        reply.view('registeredusers',
            { title: 'Searched User',
              users: foundUser,
            });
      }).catch(err => {
        reply.redirect('/');
      });
    }
  },
};

exports.viewAllUsers = {
  handler: function (request, reply) {
    User.find({}).populate('users').then(foundUser => {
        reply.view('registeredusers',
            { title: 'Users',
              users: foundUser,
              message: '',
            });
      }).catch(err => {
        reply.redirect('/');
      });
  },
};

exports.viewFriends = {
  handler: function (request, reply) {
    let userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).populate('following').then(user => {
      reply.view('friends',
          {
            title: 'Friends',
            users: user.following,
          });
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

exports.viewOtherUsers = {
  handler: function (request, reply) {
    let userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).populate('following').then(user => {
      User.find({}).populate('users').then(allUser => {
        let i = 0;
        let otherUser = allUser;
        user.following.forEach(currentUser => {
          for (let i = 0; i < otherUser.length; i++) {
            if (currentUser.email === otherUser[i].email) {
              otherUser.splice(i, 1);
              break;
            }
          }
        });
        let mili = 500;
        setTimeout(function () {
          reply.view('otherUser',
              { title: 'Other User',
                users: otherUser,
              });
        }, mili);

      }).catch(err => {
        reply.redirect('/');
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

exports.follow = {
  handler: function (request, reply) {
    let userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).populate('following').then(user => {
      User.findOne({ _id: request.params.id }).then(foundUser => {
        foundUser.followers = foundUser.followers + 1;
        foundUser.save();
        user.following.push(foundUser._id);
        return user.save();
      }).then(newFollower => {
        reply.redirect('/friends');
      }).catch(err => {
        reply.redirect('/');
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

exports.unfollow = {
  handler: function (request, reply) {
    let userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).populate('following').then(user => {
        let j = null;
        User.findOne({ _id: request.params.id }).then(foundUser => {
          foundUser.followers = foundUser.followers - 1;
          foundUser.save();
          for (let i = 0; i < user.following.length; i++) {
            if (user.following[i].email === foundUser.email) {
              j = i;
              break;
            }
          }

          if (j != null) {
            user.following.splice(j, 1);
            return user.save().then(newView => {
              reply.redirect('/friends');
            });

          } else {
            reply.redirect('/registeredusers');
          }
        }).catch(err => {
          reply.redirect('/');
        });
      }).catch(err => {
      reply.redirect('/');
    });
  },
};

exports.changeStatus = {
  validate: {

    payload: {
      status: Joi.string().required(),
    },

    options: {
      abortEarly: false,
    },

    failAction: function (request, reply, source, error) {
      User.find({}).then(users => {
        reply.view('tweet', {
          title: 'Invalid Status',
          users: users,
          errors: error.data.details,
        }).code(400);
      }).catch(err => {
        reply.redirect('/');
      });
    },
  },
  handler: function (request, reply) {
    let userEmail = request.auth.credentials.loggedInUser;
    let newStatus = request.payload;
    User.findOne({ email: userEmail }).then(user => {
      user.status = newStatus.status;
      user.save();
      reply.redirect('/tweet');
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

