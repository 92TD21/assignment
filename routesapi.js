const UserApi = require('./app/api/userapi');

module.exports = [
   { method: 'GET', path: '/api/user', config: UserApi.find },
  { method: 'GET', path: '/api/user/{id}', config: UserApi.findOne },
  { method: 'POST', path: '/api/user', config: UserApi.creat },
{ method: 'DELETE', path: '/api/user/{id}', config: UserApi.deleteOne },
{ method: 'DELETE', path: '/api/user', config: UserApi.deleteAll },

 ];
